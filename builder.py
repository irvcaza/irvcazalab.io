# -*- coding: utf-8 -*-
"""
Created on Tue Dec 18 10:32:45 2018

@author: IRVING.CABRERA
"""

from jinja2 import Environment, FileSystemLoader
import json
from datetime import datetime
import locale
from  os.path import join
from babel.dates import format_date


def datetimeformat(value, language, formato='%B %Y'):
    laguagesDic = {'ESP':'es_MX','ENG':'en'}
    d = datetime.strptime(value, '%Y-%m-%d')
    formatDate = format_date(d, format="MMMM yyyy", locale=laguagesDic[language])
    return formatDate

def renderPage(lang, template):
    with open(join('Data','CV_{}.json'.format(lang)), encoding='UTF-8') as f:
        data = json.load(f)
    with open(join('Data','translate{}.json'.format(lang)), encoding='UTF-8') as f:
        translate = json.load(f)

    output = template.render(data=data, lan= lang, translate= translate)  
    return output

env = Environment(loader=FileSystemLoader(r'Templates'),lstrip_blocks=True,trim_blocks=True)
env.filters['datetimeformat'] = datetimeformat
template = env.get_template(r'template.html')

output = renderPage('ESP', template)
with open(join('public','index.html'),'w', encoding='UTF-8') as f:
    f.write(output)
    
output = renderPage('ENG', template)
with open(join('public','eng.html'),'w', encoding='UTF-8') as f:
    f.write(output)


printable = env.get_template(r'printable.html')
output = renderPage('ESP', printable)
with open(join('public','printableESP.html'),'w', encoding='UTF-8') as f:
    f.write(output)

output = renderPage('ENG', printable)
with open(join('public','printableENG.html'),'w', encoding='UTF-8') as f:
    f.write(output)
