# Irving Cabrera's Personal Webpage

Professional CV responsive site for Irving Cabrera using GitLab Pages.

Actual website can be accessed [here](https://irvcaza.gitlab.io). 

Learn more about [GitLab Pages](https://pages.gitlab.io) and the [official documentation](https://docs.gitlab.com/ce/user/project/pages/).

If you like the style fell free to fork it. But please let me know to see how it turns out.

## Directory 
This project uses [Jinja2](http://jinja.pocoo.org/) template engine that takes 
information form [Data folder](https://gitlab.com/irvcaza/irvcaza.gitlab.io/tree/master/Data)
applied to templates in [Templates folder](https://gitlab.com/irvcaza/irvcaza.gitlab.io/tree/master/Templates)
and places the result in [Public folder](https://gitlab.com/irvcaza/irvcaza.gitlab.io/tree/master/public).

This allows easy modification and extension of personal information. As well allows have the information in several languages.

## Thanks to
-  [GitLab Pages](https://pages.gitlab.io) for the free hosting
-  [Bootstrap](https://getbootstrap.com/) for offering easy responsiveness
-  [Font Awesome](https://fontawesome.com/) for the free icons
-  [Nathaniel Koloc](https://www.nathanielkoloc.com/) for the original idea of a beautiful site (he's still prettier).
-  [Jinja2](http://jinja.pocoo.org/) For an excellent template engine.
-  [Thomas Hardy](http://www.thomashardy.me.uk/) Fot the template of a printable CV

